# Mapas conceptuales <br> Matemáticas
## 1. Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
<style>
 node{
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #E9BCDE
  LineThickness 1.5
  BackgroundColor #E9BCDE
  RoundCorner 40
  MaximumWidth 300
 }

 arrow{
  LineColor #8C016A
 }

 title{
  FontSize 18
 }

 header{
  FontSize 14
 }
 </style>

caption Mapa conceptual 1/3
title Conjuntos, Aplicaciones y funciones (2002)
header
Cruz Morales Jesús
endheader

 * <b>Conjuntos\n<b>y elementos
  *_ son fruto de
   * Ideas
    *_ de los
     * Matemáticos y\nfilósofos
      *_ que comparten la 
       * Inquietud
        *_ de
         * Descubrir
          *_ los complicados\nmecanismos del
           * Razonamiento\n de ahora
  *_ surge por la\nrazón de los
   * Matemáticos
    *_ de buscar la
     * Razón última
      *_ de las\ncausas del
       * Pensamiento
  * No se\ndefinen
   *_ su
    * Concepto
     *_ ya que\nes una
      * Idea\nasumida
       *_ por el
        * Observador
         *_ que la\nva a 
          * Estudiar
    * Relación
     *_ de
      * Pertenencia
       *_ esto quiere\ndecir que
        * Un elemento y\nun conjunto
         *_ siempre se\nsabe si el
          * Elemento
           *_ está dentro\no no del
            * Conjunto
   *_ ya que\nson
    * Intuitivas
  *_ con estas\npiezas
   * Elementales
    *_ se construye\ntoda una
     * Teoría racional\nabstracta
      *_ que es\ntoda la
       * Teoría de\nconjuntos
        *_ con esta aparecen\nuna serie de
         * Conceptos 
          *_ que no son más\nque basados en estos
           * Conceptos\nprimitivos
            *_ y en las
             * Definiciones que apartir\nde ellas se obtienen
  *_ conceptos y definiciones\nque aparecen
   * Inclusión de\nconjuntos
    *_ es la
     * Primera\nnoción
      *_ que hay\nentre
       * Dos\nconjuntos
        *_ que es un
         * Trasunto 
          *_ de la 
           * Relación\nde orden
            *_ que hay\nentre
             * Números
     *_ esta dice\nque
      * Un conjunto
       *_ está
        * En otro
         *_ cuando
          * Todos los\nelementos
           *_ del
            * Primero
             *_ pertenecen al
              * Segundo
  * Conjunto de parte\nde un conjunto
   *_ son
    * Conjuntos más\ncomplejos
     *_ armados\nsobre
      * Conjuntos\nelementales
  *_ operaciones con\nconjuntos
   * Intersección
    * Elementos
     *_ que pertenecen
      * Simultáneamente
       *_ a
        * Ambos\nconjuntos
   * Unión
    * Elementos
     *_ que pertenece
      * Almenos uno\nde ellos
   * Complementación
    * Conjunto
     *_ que
      * No pertenece
       *_ a un 
        * Conjunto dado
   *_ surge una
    * Serie de\nconsecuencias
     *_ cuya
      * Validez
       *_ es fruto\nde esas
        * Definiciones y\nmecanismos\ndel razonamiento
         *_ y que operan en\nel dominio de las
          * Ideas\nindependientes
           *_ de que\nhay un
            * Conjunto\npor debajo
             *_ que este dando\nsoporte a la
              * Idea\nteórica
               *_ y son válidos\npara
                * Cualquier\nconjunto
  *_ tipos de\nconjuntos
   * Conjunto\nuniversal
    *_ es un
     * Conjunto de\nreferencia
      *_ en el que\nocurre todos los
       * Casos
        *_ de la 
         * Teoría
   * Conjunto\nvacío
    *_ es el
     * Conjunto
      *_ que
       * No tiene\nelementos
  *_ su representación
   * Diagrama de Venn
    *_ en honor a
     * John Venn
    *_ este es un
     * Recurso didáctico
      *_ muy
       * Característico
    *_ ayudan a
     * Comprender
      *_ intuitivamente la
       * Posición 
        *_ de las
         * Operaciones
    *_ útil para
     * Hacerse idea
      *_ de como\nson los
       * Casos 
    *_ no es útil para
     * Demostrar
      *_ o sea como
       * Método de\ndemostración
        *_ de las
         * Propiedades sobre\nconjuntos
          *_ exclusivamente se\ntienen que usar las
           * Definiciones
  *_ cardinal\ndel conjunto
   *_ se refiere al
    * Número de\nelementos
     *_ que
      * Contiene el\nconjunto
   *_ propiedades sobre las
    * Características de\nlos cardinales
     *_ son
      * Fórmulas
       *_ muy útiles para
        * Resolver\nproblemas
         *_ sobre
          * Cardinales
     * Cardinal de\nuna unión
      *_ es
       * Igual al\ncardinal
        *_ de uno\nde los 
         * Conjuntos
          *_ más el
           * Cardinal del\nsegundo
            *_ menos el
             * Cardinal de\nla intersección
              *_ quedando que 
               * (A+B)-C
     * Acotación de\ncardinales
      *_ se pueden\nencontrar una
       * Serie de\nrelaciones
        *_ entre quién es
         * Mayor, menor\no igual
          *_ entre
           * Cardinales\nde conjuntos
  *_ relacionados directamente\ncon las
   * Aplicaciones\ny funciones
    *_ surgimiento la
     * Matemática
      *_ introduce el
       * Concepto de\ntransformación
        *_ convierte los
         * Elementos de\nun conjunto
          *_ a
           * Elementos de un\nsegundo conjunto
        *_ se debe
         * Identificar
          *_ que elemento del
           * Primero
            *_ se convierte en\nelemento del
             * Segundo
   * Aplicación
    *_ es una
     * Clase de\ntransformación
    *_ convierte a\ncada uno de los
     * Elementos
      *_ de un
       * Conjunto\norigen
        *_ en un
         * Único
          *_ elemento de un
           * Conjunto\nfinal
          *_ la idea de que sea único
           * Toma mucha\nimportancia
            *_ ya que
             * No es\naplicación
              *_ cuando
               * Un elemento\ndel primer conjunto
                *_ tiene
                 * Varios
                  *_ del segundo\nconjunto
                   * Transformados o\nrelacionados con el
               * Una determinada\ntransformación
                *_ deja de lado\no se olvida de
                 * Alguno de\nlos elementos
                  *_ del
                   * Primer\nconjunto
    *_ tipos de aplicaciones
     * Inyectiva
     * Subjetiva
     * Biyectiva
    *_ composición de\naplicaciones son
     * Transformaciones de\ntransformaciones
      *_ consiste en hacer
       * Una aplicación\nprimero
        *_ y a continuación
         * Otra\naplicación
          *_ al transformado\nmediante la
           * Primera aplicación
   * Función
    *_ se usa la palabra\nfunción para los
     * Conjuntos
      *_ que se
       * Transforman
        *_ entre 
         * Conjuntos\nde números
    *_ son
     * Aplicaciones
      *_ que como\ntraen el
       * Substrato de\nlos números
        *_ son más\nfáciles de
         * Ver
          *_ si se\npintan unos
           * Ejes\ncoordenados
            *_ como en
             * Geometría
            *_ en el eje\nde las
             * Abscisas
              *_ el
               * Conjunto\ninicial
             * Ordenadas
              *_ el
               * Conjunto\nfinal
             *_ como cada
              * Elemento del\nconjunto
               *_ tiene uno\nligado al
                * Transformado del\nsegundo conjunto
                 *_ sale una
                  * Serie de puntos
                   *_ en ese sistema\nque dan
                    * Figura
                     *_ puede ser una
                      * Línea recta
                      * Figuras complejas
                  *_ el conjunto\nde puntos cuya
                   * Primer coordenada
                    *_ es el
                     * Elemento del primer conjunto
                   * Segunda coordenada
                    *_ es el
                     * Transformado mediante ese conjunto
                  *_ a este conjunto de\npuntos se le llama
                   * Gráfica de la función
@endmindmap
```
## 2. Funciones (2010)
```plantuml
@startmindmap
<style>
 node{
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #E891D3
  LineThickness 1.5
  BackgroundColor #E891D3
  RoundCorner 40
  MaximumWidth 300
 }

 arrow{
  LineColor #8C016A
 }

 title{
  FontSize 18
 }

 header{
  FontSize 14
 }
 </style>

caption Mapa conceptual 2/3
title Funciones (2010)
header
Cruz Morales Jesús
endheader

 * <b>Función
  *_ es el
   * Corazón de\nlas matemáticas
  *_ refleja lo\nque el
   * Pensamiento
    *_ del
     * Hombre
      *_ ha hecho para\nintentar comprender su
       * Entorno
        *_ y para intentar\ncon ayuda de las
         * Matemáticas
          *_ resolver los
           * Problemas\ncotidianos
  *_ la idea\nsurge del
   * Cambio
    *_ las matemáticas\nbuscan como
     * Matematizar
      *_ para aprovechar las
       * Matemáticas que\nhay dentro del cambio
  *_ la función\nes una
   * Aplicación\nespecial
    *_ aplicación son
     * Transformaciones
      *_ de un
       * Conjunto\nen otro
    *_ son especiales\npues los
     * Conjuntos
      *_ que se están
       * Relacionando
        *_ se están\nviendo como
         * Cambia uno\nen el otro
      *_ estos son\nconjuntos de
       * Números
        *_ en especial\nel conjunto de
         * Números\nreales
  *_ utiliza la
   * Representación\ncartesiana
    *_ permite representar\nen un
     * Plano
      *_ sencillamente\nen un
       * Eje
        * Eje de\nlas x
         *_ representan el conjunto\nde números reales en
          * Horizontal
        * Eje de\nlas y
         *_ representan el conjunto\nde números reales en
          * Vertical
        *_ estos se\ncortan en
         * Ángulo\nrecto
    *_ el resultado de\nestas son
     * Imágenes
      *_ cuando estas
       * Aumentan
        *_ se le\nconoce como
         * Funciones\ncrecientes
       * Disminuyen
        *_ se le\nconoce como
         * Funciones\ndecrecientes
    *_  como es la\nfunción es su
     * Comportamiento
      *_ esto se refiere\na como se
       * Comporta\nla función
        *_ en un
         * Intervalo\nde valores
      *_ surgen
       * Máximos\nrelativos
        *_ se dice que\nha pasado por un
         * Máximo
          *_ cuando
           * Deja de\naumentar
            *_ o sea\nempieza a
             * Decrecer
       * Mínimos\nrelativos
        *_ se dice que\nha pasado por un
         * Mínimo
          *_ cuando
           * Deja de\ndisminuir
            *_ o sea\nempieza a
             * Crecer
  *_ su
   * Estudio
    *_ se debe hacer\ndentro del 
     * Ámbito
      *_ en el que se\nmueven los
       * Valores de\nlas variables
        *_ que son de
         * Interés
  * Límites
   * Continuos
    *_ cuando se\nconsideran
     * Valores de\nla variable
      *_ que están\ncerca del
       * Punto
        *_ los valores de\nla variable y sus
         * Imágenes
          *_ están cerca de\nun determinado
           * Valor
    *_ estas\ntienen
     * Buen\ncomportamiento
      *_ no produce
       * Saltos
       * Descontinuidades
    *_ son más
     * Manejables
      *_ cuando nos\nmovemos cerca de un
       * Punto de\ninterés
        *_ los valores de la\nfunción están cerca de los
         * Valores\nantigüos
   * Discontinuos
    *_ es estas
     * No hay\nlímite
      *_ de la
       * Función
  *_ se relación\ncon la
   * Derivada
    *_ esta es muy
     * Importante
      *_ y
       * Útil
    *_ nace de la
     * Reflexión
      *_ de como
       * Aproximar
        *_ una función
         * Relativamente\ncomplicada
          *_ con una
           * Función\nmás simple
            *_ estas son\nlas cuya
             * Gramática
              *_ son líneas\nrectas o sea
               * Funciones\nlineales
            *_ de la forma
             * f(x)=Ax+B
            *_ dejan a la gráfica\nde la función
             * De un lado
              *_ y tal que
               * Toque\nun punto
                *_ de manera
                 * Tangente
                  *_ es decir la
                   * Tangente a\nla curva
    *_ permite\nmedir el
     * Cambio
@endmindmap
```
## 3. La matemática del computador (2002)
```plantuml
@startmindmap
<style>
 node{
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #E5A9F8
  LineThickness 1.5
  BackgroundColor #E5A9F8
  RoundCorner 40
  MaximumWidth 300
 }

 arrow{
  LineColor #8C016A
 }

 title{
  FontSize 18
 }

 header{
  FontSize 14
 }
 </style>

caption Mapa conceptual 3/3
title La matemática del computador (2002)
header
Cruz Morales Jesús
endheader

 * <b>Matemática\n<b>del computador
  *_ la
   * Matemática
    *_ esta en el
     * Cimiento
      *_ del 
       * Funcionamiento
        *_ de los aspectos\nque hoy nos permite
         * Comunicarnos
  *_ el interés\nes poner el
   * Manifiesto
    *_ como las
     * Matemáticas
      *_ son una
       * Herramienta\nimprescindible
        *_ ya que han\nsido una
         * Base
          *_ para el desarrollo\nde los
           * Computadores
  * Aritmética del computador\no de\nPrecisión finita
   *_ ¿Cómo surge?
    * Cálculos
     *_ con
      * Números abstractos
      * Teorías
      * Abstracción de número real
     *_ no causaban\nmayor
      * Dificultad
       *_ al hacerlo con
        * Lapíz y\npapel
         *_ en el
          * Mundo de\nla mente
     *_ cuando se quiere\nhacer esos
      * Cómputos
       *_ en una
        * Máquina\nmecánica
         *_ con un número\ndado de
          * Posiciones
           *_ para
            * Representar
             *_ esos 
              * Dígitos\no cifras
               *_ surge una
                * Problemas\nprácticos
                 *_ que hacen\nreferencia a la
                  * Aritmética\nfinita
  *_ como no se\npueden representar
   * Infinitas\ncifras
    *_ de la\nrepresentación
     * Decimal
      *_ este proceso\nocasiona una
       * Serie de\nproblemas
        *_ de
         * Tipo matemático
          *_ estas son las\nideas de
           * Número aproximado
           * Error
            *_ que se\ncomete al
             * Aproximar
           * Dígitos significativos
    *_ las matemáticas\nayudan a 
     * Romper\nel problema
      *_ surgen las ideas de
       * Truncar
        *_ es 
         * Cortar un\nnúmero
          *_ unas cuantas
           * Cifras
            *_ a partir\nde una
             * Cifra dada
        *_ otro modo de\nverlo es
         * Despreciar
          *_ un determinado
           * Número de\ncifras
       * Redondear
        *_ es una
         * Especie de\ntruncamiento refinado
        *_ este trata\nde que el
         * Error\ncometido
          *_ sea a 
           * Media
            *_ es decir cuando se
             * Trunque
              *_ no sea un
               * Error
                *_ por
                 * Exceso
                 * Defecto
            *_ lo consigue
             * Despreciando y\nretocando
              *_ la
               * Última\ncifra
                *_ que no se\ndespreció en un
                 * Modo adecuado
  *_ el manejo de
   * Números grandes\ny pequeños
    *_ se manejan en
     * Forma de\nproducto
      *_ esta es un
       * Número
        *_ multiplicado\npor una
         * Potencia de 10
        *_ a este se\nle llama
         * Mantisa
      *_ permite ver la
       * Magnitud\ndel número
      *_ se dejan en
       * Notación
        * Exponencial\nnormalizada
        * Científica
        *_ son parecidas\npero
         * No se\ndeben confundir
  *_ sistemas que\nse utilizan
   * Sistema\nbinario
    *_ este es el
     * Sistema\nmás simple
      *_ formado por
       * 0 y 1
        *_ se construye\ntodo un
         * Sistema de numeración
    *_ este es el
     * Sistema básico
      *_ de la
       * Computadora
    *_ ¿Por qué se utiliza? en el
     * Mundo de la\nelectrónica
      *_ el 
       * 0
        *_ se puede identificar\ncomo la
         * Ausencia de corriente
       * 1
        *_ se puede identificar\ncomo la
         * Presencia de corriente
      *_ con estas ideas\nsimples se pueden armar
       * Estructuras complejas
        *_ dada la
         * Velocidad de\nlos computadores
          *_ esto produce un
           * Efecto apreciable
    *_ la
     * Codificación\nbinaria
      *_ permite representar\ntoda una
       * Serie de\ncaracterísticas
        *_ como las
         * Letras
         * Números
          *_ con estos se\npueden realizar
           * Operaciones\nmatemáticas
            *_ como se\nrealizan
             * Comunmente
          *_ enteros se pueden\nrepresantar en
           * Magnitud signo
           * Exceso
           * Complemento 2
           *_ estas nos\ndicen como
            * Convertir
             *_ un paso de
              * Corriente
               *_ en una\nserie de
                * Posiciones\nde memoria
                 *_ en un
                  * Ordenador
          *_ la
           * Aritmética en\npunto flotante
            *_ aborda las\nrepresentaciones de
             * Punto flotante
             * Desbordamiento
             * Punto 64
         * Signos de puntuación, etc.
    *_ para no
     * Arrastrar
      *_ una cantidad de
       * Dígitos
        *_ los
         * Informáticos
          *_ apoyados en las
           * Matemáticas
            *_ acuden a
             * Sistemas que\ncompactifiquen
              *_ esa
               * Representación
              *_ estos sistemas son
               * Sistema octal
               * Sistema hexadecimal
              *_ estos tienen\nla misión de
               * Acortar o\nagrupar
                *_ en forma de una
                 * Serie de\ncaracteres
              *_ utilizan más
               * Símbolos
                *_ para representar un
                 * Número
              *_ mayor
               * Dificultad
                *_ para representar la
                 * Aritmética en\npunto flotante
@endmindmap
```
